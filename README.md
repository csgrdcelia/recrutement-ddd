# recrutement-ddd (groupe 2)

## GIVEN 
Un candidat avec ses disponibilités, une liste de consultants recruteurs, une liste de salles
## WHEN 
La chargé de recrutement crée l'entretien
## THEN 
Un entretien est planifié avec un consultant recruteur ayant plus d'années d'expérience que le candidat et les mêmes compétences, avec des disponibilités concordantes et une salle disponible à cette date et heure
